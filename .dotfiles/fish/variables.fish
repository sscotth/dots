#!/usr/bin/env fish

# Remove fish default greeting
set --erase fish_greeting
set -Ux fish_greeting ""

set --erase DOTFILES_GIT_DIR
set --erase HOMEBREW_CASK_OPTS

set -Ux DOTFILES_GIT_DIR $HOME/.dfgit
set -Ux HOMEBREW_CASK_OPTS "--no-quarantine"
