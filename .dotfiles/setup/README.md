# dots

# macOS Setup

```
DOTFILES_GIT_DIR=$HOME/.dfgit
git --version # Triggers download of CLDT
git clone --bare https://gitlab.com/sscotth/dots $DOTFILES_GIT_DIR
git --git-dir=$DOTFILES_GIT_DIR --work-tree=$HOME checkout main
~/.dotfiles/setup/macos.sh
```
