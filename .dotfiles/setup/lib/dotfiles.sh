#!/bin/sh

# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                          /!\ Install dotfiles /!\                           #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                                                                             #
#  Elegant way to manage and share dotfiles across machines using a single    #
#  git repository (see more ~ https://bit.ly/kalkayan-df)                     #
#                                                                             #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #

# The trick behind the mangement of dotfiles is cloning it as a bare repository
# therefore, first, we need to define some arguments for the git command. The
# location for the dotfiles is under $HOME directory.

DOTFILES_REPO=https://gitlab.com/sscotth/dots
DOTFILES_GIT_DIR=$HOME/.dfgit
DOTFILES_CMD="/usr/bin/git --git-dir=$DOTFILES_GIT_DIR --work-tree=$HOME"

# Brachs gives us the functionality to create profiles of dotfiles for machines
# main is for macos, linux is for ubuntu etc.
DOTFILES_BRANCH="main"

# Bring the dotfiles from hosted repository if not already present
if [ ! -d $DOTFILES_GIT_DIR ]; then
    /usr/bin/git clone --bare $DOTFILES_REPO $DOTFILES_GIT_DIR
fi

# Reset the unstagged changes before updating (TODO: experiment with stash)
if [[ " $@ " =~ " --dotfiles-reset " ]]; then
    $DOTFILES_CMD reset --hard
fi

# Activate the profile for the current dev machine and Update the dotfiles with
# remote repository
$DOTFILES_CMD checkout $DOTFILES_BRANCH
$DOTFILES_CMD pull origin $DOTFILES_BRANCH
