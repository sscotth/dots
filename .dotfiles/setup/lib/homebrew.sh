#!/bin/sh

# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                              /!\ Homebrew /!\                               #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                                                                             #
#  Install Homebrew system package manager - Brew.sh.                         #
#                                                                             #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #

# Install brew.sh (system package manager) if already not installed
if ! brew -h 1>/dev/null; then
    cached_psudo '/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"'
fi

# Update brew to latest available version
brew update
