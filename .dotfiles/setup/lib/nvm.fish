#!/usr/bin/env fish

fisher install jorgebucaran/nvm.fish

mkdir -p $HOME/.local/share/nvm/

nvm install lts    # v14 # fermium 
nvm install erbium # v12
nvm install latest # v16 # gallium (Oct 2021)

set --universal nvm_default_version latest # auto load
