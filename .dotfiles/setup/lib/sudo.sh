#!/bin/sh

# Caches the sudo password as a variable for scripts.
# Some scripts cannot be as sudo like homebrew, but requires a password during certain tasks.
# Use psudo for those scripts.
cached_psudo() {
    echo cached_psudo $@
    base64_cmd=$(echo $@ | base64 | tr -d '\n')
    echo base64_cmd $base64_cmd

    if [ -z ${SUDOPASS+x} ]; then
        read -s -p "(cached_psudo) SUDO Password:" SUDOPASS
        export SUDOPASS=$SUDOPASS
        echo ''
    fi

    /usr/bin/expect <<EOD
    set timeout -1
    spawn $HOME/.dotfiles/setup/lib/base64_eval.sh $base64_cmd
    expect {
      "*?assword:*" { send "$SUDOPASS\n"; exp_continue }
      "*?assword for*" { send "$SUDOPASS\r"; exp_continue }
      "*Press RETURN to continue*" { send "\n"; exp_continue }
    }
    catch wait result
    exit [lindex \$result 3]
EOD
}
export -f cached_psudo

cached_sudo() {
    echo cached_sudo $@
    base64_cmd=$(echo $@ | base64 | tr -d '\n')
    echo base64_cmd $base64_cmd

    if [ -z ${SUDOPASS+x} ]; then
        read -s -p "(cached_sudo) SUDO Password:" SUDOPASS
        export SUDOPASS=$SUDOPASS
        echo ''
    fi

    echo cached_sudo $@
    base64_cmd=$(echo sudo $@ | base64 | tr -d '\n')

    /usr/bin/expect <<EOD
    set timeout -1
    spawn $HOME/.dotfiles/setup/lib/base64_eval.sh $base64_cmd

    expect {
      "*?assword:*" { send "$SUDOPASS\r"; exp_continue }
      "*?assword for*" { send "$SUDOPASS\r"; exp_continue }
      "*Press RETURN to continue*" { send "\r"; exp_continue }
    }

    catch wait result
    exit [lindex \$result 3]
EOD
}
export -f cached_sudo

cached_sudo -v
