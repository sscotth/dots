#!/usr/bin/env fish

curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher

fisher update

fisher install IlanCosman/tide
fisher install franciscolourenco/done
fisher install jorgebucaran/autopair.fish
fisher install gazorby/fish-abbreviation-tips
