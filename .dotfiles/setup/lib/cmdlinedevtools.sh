#!/bin/sh

# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                    /!\ Command Line Developer Tools /!\                     #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                                                                             #
#  Install macOS SDK, headers, and build tools such as the compilers - LLVM,  #
#  interpreters                                                               #
#                                                                             #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #



# Install Xcode-select (xcode command line tools) is not already installed
if ! xcode-select -p 1>/dev/null; then
    cached_sudo xcode-select --install
fi
