#!/bin/sh

# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                           /!\ Binaries and Casks /!\                        #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #
#                                                                             #
#  This section deals with the installation of all the binaries and casks.    #
#                                                                             #
# :-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-: #

cached_psudo 'brew bundle --file=$HOME/.dotfiles/homebrew/Brewfile.min.rb'
