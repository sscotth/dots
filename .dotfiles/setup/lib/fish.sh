#!/bin/sh

function brew_install_or_upgrade () {
  if brew ls --versions "$1" >/dev/null; then
      HOMEBREW_NO_AUTO_UPDATE=1 brew upgrade "$1" || true
  else
      HOMEBREW_NO_AUTO_UPDATE=1 brew install "$1"
  fi
}

brew_install_or_upgrade fish

WHICH_FISH=`which fish`

if ! grep $WHICH_FISH /etc/shells; then
    cached_sudo "echo ${WHICH_FISH} | sudo tee -a /etc/shells"
fi

cached_psudo chsh -s $WHICH_FISH
