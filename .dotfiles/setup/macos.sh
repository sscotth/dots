#!/bin/sh

set -Eeoux pipefail
# -E listens for ERR Traps
# -e exit immediately when a command fails. Use command || true to exempt
# -o pipefail sets the exit code of a pipeline to that of the rightmost command to exit with a non-zero status
# -u treat unset variables as an error and exit immediately. Prefer ${VAR:-$DEFAULT} or ${MY_VAR:-} if you don't want a default value.
# -x print each command before executing it.

# BASH_SOURCE http://mywiki.wooledge.org/BashFAQ/028

source "${BASH_SOURCE%/*}/lib/sudo.sh"

"${BASH_SOURCE%/*}/lib/fulldiskaccess.sh"
"${BASH_SOURCE%/*}/lib/cmdlinedevtools.sh"
"${BASH_SOURCE%/*}/lib/dotfiles.sh"
"${BASH_SOURCE%/*}/lib/cleandsstore.sh"
"${BASH_SOURCE%/*}/lib/remotelogin.sh" || true
"${BASH_SOURCE%/*}/lib/directories.sh"
"${BASH_SOURCE%/*}/lib/homebrew.sh"
"${BASH_SOURCE%/*}/lib/fish.sh"
"${BASH_SOURCE%/*}/lib/brewbundle.sh"
"${BASH_SOURCE%/*}/lib/librewolf.sh"
"${BASH_SOURCE%/*}/lib/fisher.fish"
"${BASH_SOURCE%/*}/lib/nvm.fish"

"${BASH_SOURCE%/*}/../fish/variables.fish"
