#!/bin/sh

# BASH_SOURCE http://mywiki.wooledge.org/BashFAQ/028
"${BASH_SOURCE%/*}/macos.sh"

"${BASH_SOURCE%/*}/lib/homebrewfull.sh"
"${BASH_SOURCE%/*}/lib/librewolf.sh"
