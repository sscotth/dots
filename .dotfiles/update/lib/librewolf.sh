#!/bin/sh

WORKING_DIR="$TMPDIR/librewolf"
EXPECTED_DMG_VOLUME="/Volumes/Firefox"
EXPECTED_DMG_FILENAME="Librewolf.dmg"
EXPECTED_APP_FILENAME="LibreWolf.app"
APPLICATIONS_DIR="/Applications"

git clone --depth 1 --recursive https://gitlab.com/librewolf-community/browser/macos $WORKING_DIR
cd $WORKING_DIR
./package.sh

hdiutil attach $EXPECTED_DMG_FILENAME
rm -rf $APPLICATIONS_DIR/$EXPECTED_APP_FILENAME
cp -r $EXPECTED_DMG_VOLUME/$EXPECTED_APP_FILENAME $APPLICATIONS_DIR
hdiutil eject $EXPECTED_DMG_VOLUME

cd
rm -rf $WORKING_DIR
