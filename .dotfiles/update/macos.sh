#!/bin/sh

# BASH_SOURCE http://mywiki.wooledge.org/BashFAQ/028
"${BASH_SOURCE%/*}/lib/homebrew.sh"
"${BASH_SOURCE%/*}/lib/fisher.fish"
"${BASH_SOURCE%/*}/lib/cleandsstore.sh"
"${BASH_SOURCE%/*}/../fish/variables.fish"
