# dots

## macOS Setup

```sh
# Trigger download of CLDT
git --version
# Bare `.git` folder will be stored seperately
DOTFILES_GIT_DIR=$HOME/.dfgit
# Clone bare
git clone --bare https://gitlab.com/sscotth/dots $DOTFILES_GIT_DIR
# Checkout branch
git --git-dir=$DOTFILES_GIT_DIR --work-tree=$HOME checkout main
# Setup
~/.dotfiles/setup/macos.sh
```
