# Defined interactively
function sha256
if test -f /usr/local/opt/libressl/bin/openssl
/usr/local/opt/libressl/bin/openssl sha256 $argv
else if test -f /usr/local/opt/openssl/bin/openssl
/usr/local/opt/openssl/bin/openssl sha256 $argv
else
openssl sha256 $argv
end
end
