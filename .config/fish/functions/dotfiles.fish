function dotfiles --wraps='git --git-dir=$DOTFILES_GIT_DIR --work-tree=$HOME'
    git --git-dir=$DOTFILES_GIT_DIR --work-tree=$HOME $argv
end
