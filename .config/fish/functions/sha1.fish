# Defined interactively
function sha1
if test -f /usr/local/opt/libressl/bin/openssl
/usr/local/opt/libressl/bin/openssl sha1 $argv
else if test -f /usr/local/opt/openssl/bin/openssl
/usr/local/opt/openssl/bin/openssl sha1 $argv
else
openssl sha1 $argv
end
end
